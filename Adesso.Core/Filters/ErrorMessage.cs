﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adesso.Core.Filters
{
    public class ErrorMessage
    {
        public string Code { get; private set; }

        public string Message { get; private set; }

        public ErrorMessage(string code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}
