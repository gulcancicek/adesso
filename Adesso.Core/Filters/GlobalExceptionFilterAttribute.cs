﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Adesso.Core.Filters
{
    public class GlobalExceptionFilterAttribute : ActionFilterAttribute, IExceptionFilter
    {
        public GlobalExceptionFilterAttribute()
        {

        }

        public void OnException(ExceptionContext context)
        {
             if (context.Exception is ArgumentException argumentException)
            {

                context.Result = new ObjectResult(new ErrorMessage(string.Empty, argumentException.Message))
                {
                    StatusCode = 500,
                    DeclaredType = typeof(ArgumentException)
                };
            }

        }
        private string FormatErrorMessage(Exception ex) => $"{ex.Message}{Environment.NewLine}StackTrace: {ex.StackTrace}";
    }
}
