﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Adesso.Service
{
    public class Teams
    {
        public static readonly Dictionary<string, List<string>> teamsByCountry = new Dictionary<string, List<string>>
    {
        { "Türkiye", new List<string> { "Adesso İstanbul", "Adesso Ankara", "Adesso İzmir", "Adesso Antalya" } },
        { "Almanya", new List<string> { "Adesso Berlin", "Adesso Frankfurt", "Adesso Münih", "Adesso Dortmund" } },
        { "Fransa", new List<string> { "Adesso Paris", "Adesso Marsilya", "Adesso Nice", "Adesso Lyon" } },
        { "Hollanda", new List<string> { "Adesso Amsterdam", "Adesso Rotterdam", "Adesso Lahey", "Adesso Eindhoven" } },
        { "Portekiz", new List<string> { "Adesso Lisbon", "Adesso Porto", "Adesso Braga", "Adesso Coimbra" } },
        { "İtalya", new List<string> { "Adesso Roma", "Adesso Milano", "Adesso Venedik", "Adesso Napoli" } },
        { "İspanya", new List<string> { "Adesso Sevilla", "Adesso Madrid", "Adesso Barselona", "Adesso Granada" } },
        { "Belçika", new List<string> { "Adesso Brüksel", "Adesso Brugge", "Adesso Gent", "Adesso Anvers" } }
    };

        public static readonly List<string> Countries = new List<string>
        {
            "Türkiye", "Almanya", "Fransa", "Hollanda", "Portekiz", "İtalya", "İspanya", "Belçika"
        };
        public class Team
        {
            public string CountryName { get; set; }
            public string Name { get; set; }
        }

        public class Group
        {
            public string Name { get; set; }
            public List<Team> Teams = new List<Team>();
        }
        Dictionary<string, Group> _groups = new Dictionary<string, Group>();
        public class Response
        {
            public Dictionary<string, List<Team>> groups { get; set; }
        }
    }
}
