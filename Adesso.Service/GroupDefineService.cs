﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using static Adesso.Service.Teams;
using Group = Adesso.Service.Teams.Group;

namespace Adesso.Service
{
    public class GroupDefineService
    {
        private readonly Random random = new Random();
    
        private static List<string> DrawnTeams = new List<string>();
        private static Dictionary<string, string> Groups = new Dictionary<string, string>();
        public Response DefineGroups(int n,string username)
        {
            Response response = new Response();
            var random = new Random();
            Dictionary<string, List<Team>> groupList = new Dictionary<string, List<Team>>();
             Dictionary<string, List<string>> TeamsByCountry = teamsByCountry;
            int teamsInGroup = n == 4 ? 8 : 4;

            for (int i = 0; i < n; i++)
            {
                var availableCountries = new List<string>(Countries);
                for (int b = 0; b < teamsInGroup; b++)
                {
                    var groupName = ((char)('A' + b)).ToString();
                    Group group = new Group();
                    group.Name=groupName;

                    var selectedCountry = availableCountries[random.Next(availableCountries.Count)];
                    availableCountries.Remove(selectedCountry);

                    var teamList = TeamsByCountry[selectedCountry]; //takımı bulduk ekle
                    var team = teamList.First();
                    teamList.Remove(team);
                    TeamsByCountry[selectedCountry]=teamList;
                    group.Teams.Add(new Team() { Name=team });
                    List<Team> teams = new List<Team>();
                    if (groupList.TryGetValue(groupName, out teams))
                    {
                        if (teams != null)
                        {
                            teams.Add(new Team() { Name=team });
                        }
                    }
                    else {
                       teams = new List<Team>();
                        teams.Add(new Team() { Name=team });
                        groupList.Add(groupName, teams);
                            }
                }
            
                
            }

            response.groups = groupList;
            return response;
        }
    }
}
