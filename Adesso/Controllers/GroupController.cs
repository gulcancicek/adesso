﻿using Adesso.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net;
using System;
using System.Collections.Generic;

namespace Adesso.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GroupController : ControllerBase
    {
        GroupDefineService groupDefineService = new GroupDefineService();

        [ProducesResponseType((int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError)]
        [HttpGet("groups")]
        public ActionResult GetGroups(int groupCount, string name)
        {
           var result= groupDefineService.DefineGroups(groupCount,name);
            return Ok(result);
        }

     
    }
}
